package com.hoangnx.differentpokemon.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.hoangnx.differentpokemon.R;
import com.hoangnx.differentpokemon.model.PokemonModel;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChoosePokemonAdapter extends RecyclerView.Adapter<ChoosePokemonAdapter.ViewHolder> {

    private List<PokemonModel> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context context;
    private int heightItem;

    // data is passed into the constructor
    public ChoosePokemonAdapter(Context context, List<PokemonModel> data, int heightItem, ItemClickListener mClickListener) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.heightItem = heightItem;
        this.mClickListener = mClickListener;
        this.context = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_choose_pokemon, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
//        holder.bgrPokemon.setBackgroundColor(Color.parseColor(mData.get(position).getColor()));
//        holder.imgPokemon.setImageResource(mData.get(position).getImg());
        holder.bgrPokemon.getLayoutParams().height = heightItem;

        holder.imgCircle.setImageResource(mData.get(position).getColor());
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgPokemon;
        LinearLayout bgrPokemon;
        CircleImageView imgCircle;

        ViewHolder(View itemView) {
            super(itemView);
            imgPokemon = itemView.findViewById(R.id.img_pokemon);
            bgrPokemon = itemView.findViewById(R.id.bgr_pokemon);
            imgCircle = itemView.findViewById(R.id.img_circle);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition(), mData.get(getAdapterPosition()));
        }
    }


    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position, PokemonModel data);
    }
}