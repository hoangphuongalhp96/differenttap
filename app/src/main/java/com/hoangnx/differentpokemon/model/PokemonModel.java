package com.hoangnx.differentpokemon.model;

public class PokemonModel {
    private int color;
    private int img;
    private boolean isCorrect;

    public PokemonModel(int color, int img, boolean isCorrect) {
        this.color = color;
        this.img = img;
        this.isCorrect = isCorrect;
    }

    public PokemonModel() {
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }
}
