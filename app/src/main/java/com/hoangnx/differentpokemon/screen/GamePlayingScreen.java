package com.hoangnx.differentpokemon.screen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.hoangnx.differentpokemon.R;
import com.hoangnx.differentpokemon.adapter.ChoosePokemonAdapter;
import com.hoangnx.differentpokemon.model.PokemonModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.hoangnx.differentpokemon.screen.MainActivity.listColor;
import static com.hoangnx.differentpokemon.screen.MainActivity.listImgPokemon;

public class GamePlayingScreen extends AppCompatActivity {

    private RecyclerView rcvChoose;
    private ChoosePokemonAdapter adapter;
    private List<PokemonModel> listPokemon = new ArrayList<>();

    private int collum = 2;
    private int numBgr = 0;
    private int tempImg = 0;
    private int tempBgr = 0;
    private boolean isSetCorrected;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_playing_screen);
        initView();
    }

    private void initView() {
        rcvChoose = findViewById(R.id.rcv_choose);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        rcvChoose.getLayoutParams().height = displayMetrics.widthPixels - 100;
        rcvChoose.getLayoutParams().width = displayMetrics.widthPixels - 100;
        initRcv();
    }

    private void initRcv() {
        listPokemon.clear();
        Random rand = new Random();
        int min = 0;
        int max = (collum*collum)-1;
        int correctNum = rand.nextInt(max + 1 - min) + min;
//        int correctNum = 0;

        Log.d("correctNum: ", String.valueOf(correctNum));
        Random randImg = new Random();
        int minImg = 0;
        int maxImg = listImgPokemon.size()-1;
        int numImg = randImg.nextInt(maxImg + 1 - minImg) + minImg;

        Random rand2 = new Random();
        int a = 0;
        int b = 1;
        int c = rand2.nextBoolean() ? a : b;
        for (int i = 0; i< collum*collum; i ++) {
            if (i == correctNum) {
                if (i == 0) {
                    tempImg = listImgPokemon.get(numImg);
                    tempBgr = listColor.get(numBgr+1);
                    isSetCorrected = true;
                    listPokemon.add(new PokemonModel(listColor.get(numBgr),listImgPokemon.get(numImg),true));
                } else {
                    checkImgBgr(tempImg, tempBgr, c, true);
                }
            } else {
                if (i == 0) {
                    tempImg = listImgPokemon.get(numImg);
                    tempBgr = listColor.get(numBgr);
                    listPokemon.add(new PokemonModel(listColor.get(numBgr),listImgPokemon.get(numImg),false));
                } else {
                    checkImgBgr(tempImg, tempBgr, c, false);
                }
            }
        }

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, collum);
        gridLayoutManager.setSpanCount(collum);
        rcvChoose.setLayoutManager(gridLayoutManager);
        rcvChoose.setNestedScrollingEnabled(false);
        int heightItem = rcvChoose.getLayoutParams().height/collum;
        adapter = new ChoosePokemonAdapter(this, listPokemon, heightItem, new ChoosePokemonAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position, PokemonModel data) {
                collum+=1;
                initRcv();
            }
        });
        rcvChoose.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        if (numBgr < listColor.size()) {
            if (numBgr == listColor.size() - 1){
                numBgr = 0;
                return;
            }
            numBgr += 1;
        }

    }

    private void checkImgBgr(int tempImg, int tempBgr, int c, boolean isCorrect) {
        PokemonModel pokemonModel = new PokemonModel();
        pokemonModel.setCorrect(isCorrect);

        switch (tempBgr) {
            case R.color.color_ingame_1_1:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_1_1);
                } else
                    pokemonModel.setColor(R.color.color_ingame_1_2);
                break;

            case R.color.color_ingame_1_2:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_1_2);
                } else
                    pokemonModel.setColor(R.color.color_ingame_1_1);
                break;
            case R.color.color_ingame_2_1:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_2_1);
                } else
                    pokemonModel.setColor(R.color.color_ingame_2_2);
                break;

            case R.color.color_ingame_2_2:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_2_2);
                } else
                    pokemonModel.setColor(R.color.color_ingame_2_1);
                break;
            case R.color.color_ingame_3_1:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_3_1);
                } else
                    pokemonModel.setColor(R.color.color_ingame_3_2);
                break;

            case R.color.color_ingame_3_2:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_3_2);
                } else
                    pokemonModel.setColor(R.color.color_ingame_3_1);
                break;
            case R.color.color_ingame_4_1:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_4_1);
                } else
                    pokemonModel.setColor(R.color.color_ingame_4_2);
                break;

            case R.color.color_ingame_4_2:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_4_2);
                } else
                    pokemonModel.setColor(R.color.color_ingame_4_1);
                break;
            case R.color.color_ingame_5_1:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_5_1);
                } else
                    pokemonModel.setColor(R.color.color_ingame_5_2);
                break;

            case R.color.color_ingame_5_2:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_5_2);
                } else
                    pokemonModel.setColor(R.color.color_ingame_5_1);
                break;
            case R.color.color_ingame_6_1:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_6_1);
                } else
                    pokemonModel.setColor(R.color.color_ingame_6_2);
                break;

            case R.color.color_ingame_6_2:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_6_2);
                } else
                    pokemonModel.setColor(R.color.color_ingame_6_1);
                break;
            case R.color.color_ingame_7_1:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_7_1);
                } else
                    pokemonModel.setColor(R.color.color_ingame_7_2);
                break;

            case R.color.color_ingame_7_2:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_7_2);
                } else
                    pokemonModel.setColor(R.color.color_ingame_7_1);
                break;
            case R.color.color_ingame_8_1:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_8_1);
                } else
                    pokemonModel.setColor(R.color.color_ingame_8_2);
                break;

            case R.color.color_ingame_8_2:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_8_2);
                } else
                    pokemonModel.setColor(R.color.color_ingame_8_1);
                break;
            case R.color.color_ingame_9_1:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_9_1);
                } else
                    pokemonModel.setColor(R.color.color_ingame_9_2);
                break;

            case R.color.color_ingame_9_2:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_9_2);
                } else
                    pokemonModel.setColor(R.color.color_ingame_9_1);
                break;
            case R.color.color_ingame_10_1:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_10_1);
                } else
                    pokemonModel.setColor(R.color.color_ingame_10_2);
                break;

            case R.color.color_ingame_10_2:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_10_2);
                } else
                    pokemonModel.setColor(R.color.color_ingame_10_1);
                break;
            case R.color.color_ingame_11_1:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_11_1);
                } else
                    pokemonModel.setColor(R.color.color_ingame_11_2);
                break;

            case R.color.color_ingame_11_2:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_11_2);
                } else
                    pokemonModel.setColor(R.color.color_ingame_11_1);
                break;
            case R.color.color_ingame_12_1:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_12_1);
                } else
                    pokemonModel.setColor(R.color.color_ingame_12_2);
                break;

            case R.color.color_ingame_12_2:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_12_2);
                } else
                    pokemonModel.setColor(R.color.color_ingame_12_1);
                break;
            case R.color.color_ingame_13_1:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_13_1);
                } else
                    pokemonModel.setColor(R.color.color_ingame_13_2);
                break;

            case R.color.color_ingame_13_2:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_13_2);
                } else
                    pokemonModel.setColor(R.color.color_ingame_13_1);
                break;
            case R.color.color_ingame_14_1:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_14_1);
                } else
                    pokemonModel.setColor(R.color.color_ingame_14_2);
                break;

            case R.color.color_ingame_14_2:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_14_2);
                } else
                    pokemonModel.setColor(R.color.color_ingame_14_1);
                break;
            case R.color.color_ingame_15_1:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_15_1);
                } else
                    pokemonModel.setColor(R.color.color_ingame_15_2);
                break;

            case R.color.color_ingame_15_2:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_15_2);
                } else
                    pokemonModel.setColor(R.color.color_ingame_15_1);
                break;
            case R.color.color_ingame_16_1:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_16_1);
                } else
                    pokemonModel.setColor(R.color.color_ingame_16_2);
                break;

            case R.color.color_ingame_16_2:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_16_2);
                } else
                    pokemonModel.setColor(R.color.color_ingame_16_1);
                break;
            case R.color.color_ingame_17_1:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_17_1);
                } else
                    pokemonModel.setColor(R.color.color_ingame_17_2);
                break;

            case R.color.color_ingame_17_2:
                if (!isCorrect) {
                    pokemonModel.setColor(R.color.color_ingame_17_2);
                } else
                    pokemonModel.setColor(R.color.color_ingame_17_1);
                break;
        }
        listPokemon.add(pokemonModel);
    }
}
