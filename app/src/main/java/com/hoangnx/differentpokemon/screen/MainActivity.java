package com.hoangnx.differentpokemon.screen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.hoangnx.differentpokemon.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnStart;
    public static List<Integer> listImgPokemon = new ArrayList<>();
    public static List<Integer> listColor = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initData();
    }

    private void initData() {
        listImgPokemon.add(R.drawable.ic_abra);
        listImgPokemon.add(R.drawable.ic_bellsprout);
        listImgPokemon.add(R.drawable.ic_bullbasaur);
        listImgPokemon.add(R.drawable.ic_caterpie);
        listImgPokemon.add(R.drawable.ic_charmander);
        listImgPokemon.add(R.drawable.ic_dratini);
        listImgPokemon.add(R.drawable.ic_jigglypuff);
        listImgPokemon.add(R.drawable.ic_mankey);
        listImgPokemon.add(R.drawable.ic_meowth);
        listImgPokemon.add(R.drawable.ic_pidgey);
        listImgPokemon.add(R.drawable.ic_pikachu);
        listImgPokemon.add(R.drawable.ic_psyduck);
        listImgPokemon.add(R.drawable.ic_rattata);
        listImgPokemon.add(R.drawable.ic_venonat);
        listImgPokemon.add(R.drawable.ic_weedle);
        listImgPokemon.add(R.drawable.ic_zubat);

        listColor.add(R.color.color_ingame_1_1);
        listColor.add(R.color.color_ingame_1_2);

        listColor.add(R.color.color_ingame_2_1);
        listColor.add(R.color.color_ingame_2_2);

        listColor.add(R.color.color_ingame_3_1);
        listColor.add(R.color.color_ingame_3_2);

        listColor.add(R.color.color_ingame_4_1);
        listColor.add(R.color.color_ingame_4_2);

        listColor.add(R.color.color_ingame_5_1);
        listColor.add(R.color.color_ingame_5_2);

        listColor.add(R.color.color_ingame_6_1);
        listColor.add(R.color.color_ingame_6_2);

        listColor.add(R.color.color_ingame_7_1);
        listColor.add(R.color.color_ingame_7_2);

        listColor.add(R.color.color_ingame_8_1);
        listColor.add(R.color.color_ingame_8_2);

        listColor.add(R.color.color_ingame_9_1);
        listColor.add(R.color.color_ingame_9_2);

        listColor.add(R.color.color_ingame_10_1);
        listColor.add(R.color.color_ingame_10_2);

        listColor.add(R.color.color_ingame_11_1);
        listColor.add(R.color.color_ingame_11_2);

        listColor.add(R.color.color_ingame_12_1);
        listColor.add(R.color.color_ingame_12_2);

        listColor.add(R.color.color_ingame_13_1);
        listColor.add(R.color.color_ingame_13_2);

        listColor.add(R.color.color_ingame_14_1);
        listColor.add(R.color.color_ingame_14_2);

        listColor.add(R.color.color_ingame_15_1);
        listColor.add(R.color.color_ingame_15_2);

        listColor.add(R.color.color_ingame_16_1);
        listColor.add(R.color.color_ingame_16_2);

        listColor.add(R.color.color_ingame_17_1);
        listColor.add(R.color.color_ingame_17_2);
    }

    private void initView() {
        btnStart = findViewById(R.id.btn_start);
        btnStart.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_start:
                Intent iPlay = new Intent(this, GamePlayingScreen.class);
                startActivity(iPlay);
                break;
        }
    }
}
